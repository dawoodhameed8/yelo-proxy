const express = require("express");
const { createProxyMiddleware } = require("http-proxy-middleware");
const morgan = require("morgan");
const cronjob = require("node-cron");
const axios = require("axios");
const axios_retry = require("axios-retry");
const Cache = require("node-cache");
const xl = require("excel4node");
const sgMail = require("@sendgrid/mail");
require("dotenv").config();
const my_cache = new Cache({ checkperiod: 600, deleteOnExpire: false });
const fs = require("fs");
const app = express();

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

//prod
let marketplace_user_id = 742314;
let api_key = "f7e9bc3d6eeabfeee62bed0db35f3f0f";
let errorObjects = [];
my_cache.set("errorObjects", errorObjects, 21600);
//development
//let api_key = "232ba55a46c65897eb6088e4e7b8bcd5";
//let marketplace_user_id = 1150119;

//forward request to yelo endpoints

//this will log all incoming requests
app.use(morgan());
axios_retry(axios, { retries: 3 })

//where * matches any characters
app.post(
  "/open/*",
  createProxyMiddleware({
    target: "https://api.yelo.red",
    changeOrigin: true,
  })
);

app.get(
  "/open/*",
  createProxyMiddleware({
    target: "https://api.yelo.red",
    changeOrigin: true,
  })
);

app.post("/mail-check", (req, res, next) => {
  const pathToAttachment = `${__dirname}/Logs.xlsx`;
  const attachment = fs.readFileSync(pathToAttachment).toString("base64");
  fs.readFile(pathToAttachment, {}, (err, data) => {
    console.log(data.toString("base64").length, "checking");
  })
  console.log(attachment.length);
  const msg = {
    to: "dawoodhameed8@gmail.com",
    from: "support@breaze.co.za",
    subject: new Date(),
    text: "Email Check",
  };

  sgMail
    .send(msg)
    .then((response) => {
      console.log(response);
      return res.status(200).json({});
    })
    .catch((err) => {
      console.log(err);
      console.log("error occured");
      console.log(err.response.body);
      return res.status(500).json({});
    });
})

app.get("/proxy", (req, res) => {
  res.status(200).json({ message: "Proxy Server Working! Cheez" });
});

app.post("/update-products", async (req, res) => {
  let apiErrorObjects = [];
  //generate new cache object
  console.log("api");
  setTimeout(() => {
    console.log("test");

    let wb = new xl.Workbook();
    let ws = wb.addWorksheet("failed_updates");
    let style = wb.createStyle({
      font: {
        size: 18,
      },
    });
    let heading = wb.createStyle({
      font: {
        size: 12,
      },
    });
    try {
      ws.cell(1, 1).string("Reporting for Failed Updates").style(style);
      ws.cell(2, 1).string("Product Name").style(heading);
      ws.cell(2, 2).string("Product ID").style(heading);
      ws.cell(2, 3).string("Merchant ID").style(heading);
      ws.cell(2, 4).string("Marketplace ID").style(heading);
      ws.cell(2, 5).string("Error Message").style(heading);
      ws.cell(2, 6).string("JSON Body").style(heading);

      let cellNumber = 4;

      for (let i = 0; i < apiErrorObjects.length; i++) {
        ws.cell(cellNumber, 1).string(apiErrorObjects[i].name);
        ws.cell(cellNumber, 2).number(apiErrorObjects[i].product_id);
        ws.cell(cellNumber, 3).number(apiErrorObjects[i].user_id);
        ws.cell(cellNumber, 4).number(apiErrorObjects[i].marketplace_user_id);
        ws.cell(cellNumber, 5).string(apiErrorObjects[i].message);
        ws.cell(cellNumber, 6).string(JSON.stringify(apiErrorObjects[i]));
        cellNumber++;
      }
      wb.write(`Logs.xlsx`);
    } catch (err) {
      console.log(err);
    }

    console.log("error object");

    const pathToAttachment = `${__dirname}/Logs.xlsx`;
    const attachment = fs.readFileSync(pathToAttachment).toString("base64");

    const msg = {
      to: "avi@breaze.co.za",
      from: "support@breaze.co.za",
      subject: new Date().toDateString() + " Logs",
      text: "Failed Updates",
      attachments: [
        {
          content: attachment,
          filename: "Logs.xlsx",
          type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
          disposition: "attachment",
        },
      ],
    };

    sgMail
      .send(msg)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
        console.log("error occured");
        console.log(err.response.body);
      });
  }, 9000000);

  try {
    //get all products of master store every six hour
    let masterStore = await axios.post(
      "https://api.yelo.red/open/product/getAllProducts",
      {
        api_key: api_key,
        marketplace_user_id: marketplace_user_id,
        user_type: 2,
        user_id: marketplace_user_id,
        start: 0,
        length: 1000,
        sSearch: "",
        sortCol: "1",
        sortDir: "1",
        get_all_enabled_products: 1,
        language: "en",
      }
    );
    console.log(masterStore.data.data.length);
    console.log(masterStore.data.iTotalRecords);
    //get all merchant list
    let merchantList = await axios.post(
      "https://api.yelo.red/open/marketplace/getMerchantList",
      {
        api_key: api_key,
        marketplace_user_id: marketplace_user_id,
        start: 0,
        length: 2000,
        sSearch: "",
        sortCol: "1",
        sortDir: "1",
      }
    );
    res.status(200).json({ message: "Product Update Started!", status: true });
    console.log(merchantList.data.data.length);
    for (let i = 0; i <= merchantList.data.data.length; i++) {
      //console.log(merchantList.data.data[i]);
      //filter out master store
      //get products of merchant
      console.log(merchantList.data.data[i].user_id);

      try {
        let merchantStoreProducts = await axios.post(
          "https://api.yelo.red/open/product/getAllProducts",
          {
            api_key: api_key,
            marketplace_user_id: marketplace_user_id,
            user_type: 2,
            user_id: merchantList.data.data[i].user_id,
            start: 0,
            length: 2000,
            sSearch: "",
            sortCol: "1",
            sortDir: "1",
            get_all_enabled_products: 1,
            language: "en",
          }
        );
        //check one by one product of merchant store with master store and update it
        //console.log(merchantStoreProducts.data.data.length)
        if (
          merchantStoreProducts.data.data.length > 0 &&
          merchantList.data.data[i].user_id !== marketplace_user_id
        ) {
          //master store length
          for (let j = 0; j < masterStore.data.data.length; j++) {
            //console.log(masterStore.data.data[j]);
            for (let k = 0; k < merchantStoreProducts.data.data.length; k++) {
              //merchant store
              if (
                masterStore.data.data[j].sku ===
                merchantStoreProducts.data.data[k].sku
              ) {
                //update product
                console.log(true, k);
                let productToBeUpdated = { ...masterStore.data.data[j] };
                delete productToBeUpdated.service_time;
                delete productToBeUpdated.is_agents_on_product_tags_enabled;
                delete productToBeUpdated.agent_id;
                delete productToBeUpdated.customization;
                delete productToBeUpdated.enable_tookan_agent;
                productToBeUpdated.api_key = api_key;
                productToBeUpdated.marketplace_user_id = marketplace_user_id;
                productToBeUpdated.user_id = merchantList.data.data[i].user_id;
                productToBeUpdated.product_id =
                  merchantStoreProducts.data.data[k].product_id;
                productToBeUpdated.multi_image_url = [
                  masterStore.data.data[j].image_url,
                ];
                productToBeUpdated.description_json = {
                  en: masterStore.data.data[j].description
                }
                productToBeUpdated.name_json = {
                  en: masterStore.data.data[j].name
                }
                //name
                console.log(JSON.stringify(productToBeUpdated));
                //console.log(merchantStoreProducts.data.data[k]);

                axios
                  .post("https://api.yelo.red/open/product/edit", {
                    ...productToBeUpdated,
                  })
                  .then((data) => {
                    console.log(data.data);
                  })
                  .catch((error) => {
                    console.log(error);
                    //console.log(error.config.data)
                    let data = JSON.parse(error.config.data);
                    //make an excel sheet for the products that have not been updated
                    //object for error reporting
                    let errorObj = {
                      ...data,
                      message: error.message,
                    };
                    apiErrorObjects = [...apiErrorObjects, errorObj];
                    //console.log(error);
                  });
              } else {
                //create new one
                continue;
              }
              //console.log(merchantStoreProducts.data.data[k])
            }
          }
        } else {
          continue;
        }
      } catch (error) {
        console.log(error);
      }
    }
  } catch (error) {
    console.log(error);
  }
});

/*
//for sending excel reporting mails
//run at this minute of every hour
cronjob.schedule(
  "30 22 * * *", 
  () => {
  console.log("test");
  let data = my_cache.get("errorObjects");

  let wb = new xl.Workbook();
  let ws = wb.addWorksheet("failed_updates");
  let style = wb.createStyle({
    font: {
      size: 18,
    },
  });
  let heading = wb.createStyle({
    font: {
      size: 12,
    },
  });
  try {
    ws.cell(1, 1).string("Reporting for Failed Updates").style(style);
    ws.cell(2, 1).string("Product Name").style(heading);
    ws.cell(2, 2).string("Product ID").style(heading);
    ws.cell(2, 3).string("Merchant ID").style(heading);
    ws.cell(2, 4).string("Marketplace ID").style(heading);
    ws.cell(2, 5).string("Error Message").style(heading);
    ws.cell(2, 6).string("JSON Body").style(heading);

    let cellNumber = 4;

    for (let i = 0; i < data.length; i++) {
      ws.cell(cellNumber, 1).string(data[i].name);
      ws.cell(cellNumber, 2).number(data[i].product_id);
      ws.cell(cellNumber, 3).number(data[i].user_id);
      ws.cell(cellNumber, 4).number(data[i].marketplace_user_id);
      ws.cell(cellNumber, 5).string(data[i].message);
      ws.cell(cellNumber, 6).string(JSON.stringify(data[i]));
      cellNumber++;
    }
    wb.write(`Logs.xlsx`);
  } catch (err) {
    console.log(err);
  }

  console.log("error object");
  console.log(data);

  const pathToAttachment = `${__dirname}/Logs.xlsx`;
  const attachment = fs.readFileSync(pathToAttachment).toString("base64");

  const msg = {
    to: "avi@breaze.co.za",
    from: "support@breaze.co.za",
    subject: new Date().toDateString() + " Logs",
    text: "Failed Updates",
    attachments: [
      {
        content: attachment,
        filename: "Logs.xlsx",
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        disposition: "attachment",
      },
    ],
  };

  console.log("sgmail")

  sgMail
    .send(msg)
    .then((res) => {
      console.log(res);
    })
    .catch((err) => {
      console.log(err);
      console.log("error occured");
      console.log(err.response.body);
    });
});

/*
//product update automation
cronjob.schedule(
  "1 20 * * *",
  async () => {
    console.log("cronjob running");
    try {
      //get all products of master store every six hour
      let masterStore = await axios.post(
        "https://api.yelo.red/open/product/getAllProducts",
        {
          api_key: api_key,
          marketplace_user_id: marketplace_user_id,
          user_type: 2,
          user_id: marketplace_user_id,
          start: 0,
          length: 1000,
          sSearch: "",
          sortCol: "1",
          sortDir: "1",
          get_all_enabled_products: 1,
          language: "en",
        }
      );
      console.log(masterStore.data.data.length);
      console.log(masterStore.data.iTotalRecords);
      //get all merchant list
      let merchantList = await axios.post(
        "https://api.yelo.red/open/marketplace/getMerchantList",
        {
          api_key: api_key,
          marketplace_user_id: marketplace_user_id,
          start: 0,
          length: 2000,
          sSearch: "",
          sortCol: "1",
          sortDir: "1",
        }
      );
      console.log(merchantList.data.data.length);
      for (let i = 0; i < merchantList.data.data.length; i++) {
        //console.log(merchantList.data.data[i]);
        //filter out master store
        //get products of merchant
        console.log(merchantList.data.data[i].user_id);

        try {
          let merchantStoreProducts = await axios.post(
            "https://api.yelo.red/open/product/getAllProducts",
            {
              api_key: api_key,
              marketplace_user_id: marketplace_user_id,
              user_type: 2,
              user_id: merchantList.data.data[i].user_id,
              start: 0,
              length: 2000,
              sSearch: "",
              sortCol: "1",
              sortDir: "1",
              get_all_enabled_products: 1,
              language: "en",
            }
          );
          //check one by one product of merchant store with master store and update it
          //console.log(merchantStoreProducts.data.data.length)
          if (
            merchantStoreProducts.data.data.length > 0 &&
            merchantList.data.data[i].user_id !== marketplace_user_id
          ) {
            //master store length
            for (let j = 0; j < masterStore.data.data.length; j++) {
              //console.log(masterStore.data.data[j]);
              for (let k = 0; k < merchantStoreProducts.data.data.length; k++) {
                //merchant store
                if (
                  masterStore.data.data[j].name.toLowerCase() ===
                  merchantStoreProducts.data.data[k].name.toLowerCase()
                ) {
                  //update product
                  console.log(true, k);
                  let productToBeUpdated = { ...masterStore.data.data[j] };
                  delete productToBeUpdated.service_time;
                  delete productToBeUpdated.is_agents_on_product_tags_enabled;
                  delete productToBeUpdated.agent_id;
                  delete productToBeUpdated.customization;
                  delete productToBeUpdated.enable_tookan_agent;
                  productToBeUpdated.api_key = api_key;
                  productToBeUpdated.marketplace_user_id = marketplace_user_id;
                  productToBeUpdated.user_id =
                    merchantList.data.data[i].user_id;
                  productToBeUpdated.product_id =
                    merchantStoreProducts.data.data[k].product_id;
                  productToBeUpdated.multi_image_url = [
                    masterStore.data.data[j].image_url,
                  ];
                  productToBeUpdated.description_json = {
                    en: masterStore.data.data[j].description
                  }
                  productToBeUpdated.name_json = {
                    en: masterStore.data.data[j].name
                  }
                  console.log(JSON.stringify(productToBeUpdated));
                  //console.log(merchantStoreProducts.data.data[k]);

                  axios
                    .post("https://api.yelo.red/open/product/edit", {
                      ...productToBeUpdated,
                    })
                    .then((data) => {
                      console.log(data.data);
                    })
                    .catch((error) => {
                      console.log(error);

                      //console.log(error.config.data)
                      let data = JSON.parse(error.config.data);
                      //make an excel sheet for the products that have not been updated
                      //object for error reporting
                      let errorObj = {
                        ...data,
                        message: error.message,
                      };
                      let arr = my_cache.get("errorObjects");
                      my_cache.set("errorObjects", [...arr, errorObj]);
                      //console.log(error);
                    });
                } else {
                  //create new one
                  continue;
                }
                //console.log(merchantStoreProducts.data.data[k])
              }
            }
          } else {
            continue;
          }
        } catch (error) {
          console.log(error);
        }
      }
    } catch (error) {
      console.log(error);
    }
  },
  {
    timezone: "Africa/Johannesburg",
  }
);
*/


//productUpdate();
//updateUrl();
//findAndUpdateSingleProduct();

app.listen(8080, () => {
  console.log("Yelo proxy server started!");
});
