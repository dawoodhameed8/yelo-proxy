async function findAndUpdateSingleProduct() {
  try {
    //get all products of master store every six hour
    let masterStore = await axios.post(
      "https://api.yelo.red/open/product/getAllProducts",
      {
        api_key: api_key,
        marketplace_user_id: 742314,
        user_type: 2,
        user_id: 742314,
        start: 1,
        length: 2000,
        sSearch: "",
        sortCol: "1",
        sortDir: "1",
        get_all_enabled_products: 1,
        language: "en",
      }
    );
    //console.log(masterStore.data.data.length);
    //console.log(masterStore.data.iTotalRecords);
    //get all merchant list
    let merchantList = await axios.post(
      "https://api.yelo.red/open/marketplace/getMerchantList",
      {
        api_key: api_key,
        marketplace_user_id: 742314,
        start: 1,
        length: 2000,
        sSearch: "",
        sortCol: "1",
        sortDir: "1",
      }
    );
    //find product in master store
    for (let j = 0; j < masterStore.data.data.length; j++) {
      if (masterStore.data.data[j].name == "Zonnebloem Pinotage (750ml)") {
        console.log(masterStore.data.data[j]);
      }
    }
    console.log(merchantList.data.data.length);
    /*
      for(let i = 0; i < merchantList.data.data.length; i++){
        //console.log(merchantList.data.data[i]);
        //filter out master store
        //get products of merchant
        console.log(merchantList.data.data[i].user_id);
        
        try {
        let merchantStoreProducts = await axios.post(
          "https://api.yelo.red/open/product/getAllProducts",
          {
            "api_key": api_key,
            "marketplace_user_id": 742314,
            "user_type": 2,
            "user_id": merchantList.data.data[i].user_id,
            "start": 1,
            "length": 2000,
            "sSearch": "",
            "sortCol": "1",
            "sortDir": "1",
            "get_all_enabled_products": 1,
            "language": "en"
        }
        );
        //check one by one product of merchant store with master store and update it
        //console.log(merchantStoreProducts.data.data.length)
        if(merchantStoreProducts.data.data.length > 0 && merchantList.data.data[i].user_id !== 742314){
        //master store length
          for (let k = 0; k < merchantStoreProducts.data.data.length; k++) {
            //merchant store
            if (
              masterStore.data.data[j].name.toLowerCase() ===
              merchantStoreProducts.data.data[k].name.toLowerCase()
            ) {
              //update product
              //console.log(true, k);
              let productToBeUpdated = { ...masterStore.data.data[j] };
              delete productToBeUpdated.service_time;
              delete productToBeUpdated.is_agents_on_product_tags_enabled;
              delete productToBeUpdated.agent_id;
              delete productToBeUpdated.customization;
              delete productToBeUpdated.enable_tookan_agent;
              productToBeUpdated.api_key = "f7e9bc3d6eeabfeee62bed0db35f3f0f";
              productToBeUpdated.marketplace_user_id = 742314;
              productToBeUpdated.user_id = merchantList.data.data[i].user_id;
              productToBeUpdated.product_id =
                merchantStoreProducts.data.data[k].product_id;
                //productToBeUpdated.multi_image_url=[masterStore.data.data[j].image_url]
              //console.log(JSON.stringify(productToBeUpdated));
              //console.log(merchantStoreProducts.data.data[k]);
              axios
                .post("https://api.yelo.red/open/product/edit", {
                  ...productToBeUpdated,
                })
                .then((data) => {
                  console.log(data.data);
                })
                .catch((error) => {
                  console.log(error);
                });
            } else {
              //create new one
              continue;
            }
            //console.log(merchantStoreProducts.data.data[k])
        }
      } else {
        continue;
      }
      }catch(error){
        console.log(error);
      }
      }*/
  } catch (error) {
    console.log(error);
  }
}

async function updateUrl() {
  let masterStore = await axios.post(
    "https://api.yelo.red/open/product/getAllProducts",
    {
      api_key: api_key,
      marketplace_user_id: 742314,
      user_type: 2,
      user_id: 742314,
      start: 1,
      length: 2000,
      sSearch: "",
      sortCol: "1",
      sortDir: "1",
      get_all_enabled_products: 1,
      language: "en",
    }
  );
  console.log(masterStore.data.data.length);
  let image_url = "";
  for (let i = 0; i < masterStore.data.data.length; i++) {
    //console.log(masterStore.data.data[i]);
    if (
      masterStore.data.data[i].image_url &&
      masterStore.data.data[i].image_url.length > 0
    ) {
      let a = masterStore.data.data[i].image_url.split("//");
      let b = a[1].split("/");
      b[0] = "nop.liquorcity.co.za";
      a[1] = b.join("/");
      image_url = a.join("//");
    }
    console.log(image_url);
    let productToBeUpdated = { ...masterStore.data.data[i] };
    delete productToBeUpdated.service_time;
    delete productToBeUpdated.is_agents_on_product_tags_enabled;
    delete productToBeUpdated.agent_id;
    delete productToBeUpdated.customization;
    delete productToBeUpdated.enable_tookan_agent;
    productToBeUpdated.api_key = api_key;
    productToBeUpdated.marketplace_user_id = 742314;
    productToBeUpdated.user_id = 742314;
    //productToBeUpdated.image_url = image_url
    //productToBeUpdated.multi_image_url=[image_url];
    axios
      .post("https://api.yelo.red/open/product/edit", {
        ...productToBeUpdated,
      })
      .then((data) => {
        console.log(data.data);
      })
      .catch((error) => {
        console.log(error);
      });
  }
}
